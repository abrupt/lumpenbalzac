# ~/ABRÜPT/MINA TARATUTA/LUMPENBALZAC/*

La [page de ce livre](https://abrupt.cc/mina-taratuta/lumpenbalzac/) sur le réseau.

## Sur le livre

Balzac est une offrande à nos crimes. Il porte les métamorphoses de Trompe-la-Mort jusqu’au cœur de nos grognements. Nous errons sur la brèche, mais nous n’errons pas seuls. Le spectre de Vautrin tantôt nous guide, tantôt nous fait vaciller, toujours cherche à nous faire voir l’absolu de nos révoltes.

La *lumpenlittérature* est l’art de lire l’avenir parmi nos haillons, elle est la divination qui quête une théorie du renversement.


## Sur l'autrice

Née de l’est qui s’effrite, aux alentours de Tel Aviv, puis un exil parisien. Une paléographe qui vagabonde avec quelques fantômes benjaminiens.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est dédié au domaine public. Il est mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
